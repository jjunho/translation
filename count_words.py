#! /usr/env python
# encoding: utf-8

import codecs, re, sys

PAGE = 40000.00
UNIT = 250
RATE = PAGE / UNIT

FILE = sys.argv[1]
WORD = re.compile(ur"\W", re.U)

def read_file(input_file):
    lines = codecs.open(input_file, "r", "u8").read()
    return lines

def get_words(lines):
    words = [word for word in WORD.split(lines) if word]
    return words

def count_words(words):
    count = len(words)
    return count

def calculate_price(word_count):
    price = word_count * RATE
    return price

def show_on_screen(price, word_count):
    print "Word count: %s words" % int(word_count)
    print "Rate per word: %s KRW" % int(RATE)
    print "Total: %s KRW" % int(price)

if __name__ == '__main__':
    lines = read_file(FILE)
    words = get_words(lines)
    word_count = count_words(words)
    price = calculate_price(word_count)
    show_on_screen(price, word_count)

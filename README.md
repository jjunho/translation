# Calculator for translation jobs

## Usage

`$ python count_words.py original_file.txt`

## Result

    Word count: 10 words
    Rate per word: 160 KRW
    Total: 1600 KRW
